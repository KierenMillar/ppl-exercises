child_of(emmeline, frank).
child_of(harold,frank).
child_of(amelia,frank).
child_of(chris,amelia).
child_of(chris,john).
child_of(brendon,chris).
child_of(emlyn,chris).
child_of(brendon,elizabeth).
child_of(emlyn,elizabeth).
child_of(les,maurice).
child_of(irene,maurice).
child_of(margaret,irene).
child_of(elizabeth,irene).
child_of(elizabeth,george).
child_of(margaret,george).
child_of(nick,margaret).
child_of(louise,margaret).
child_of(rebecca,margaret).
child_of(rebecca,peter).
child_of(louise,peter).
child_of(nick,peter).

male(frank).
male(john).
male(harold).
male(chris).
male(brendon).
male(emlyn).
male(maurice).
male(george).
male(les).
male(peter).
male(nick).
female(amelia).
female(emmeline).
female(elizabeth).
female(irene).
female(margaret).
female(rebecca).
female(louise).

daughter_of(X,Y) :- child_of(X,Y), female(X).
uncle_of(X,Y) :- child_of(Y,Z), child_of(Z,G), child_of(X,G), male(X), X \== Z.
father(X,Y) :- child_of(Y,X), male(X).
grandfather(X,Y) :- child_of(Y,Z), child_of(Z,X), male(X).
niece_of(X,Y) :- female(X), child_of(Y,Z), child_of(P,Z), child_of(X,P), P\==Y.



