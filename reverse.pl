reverse(L,[K]):- L==K.
reverse([Head1|Tail1],[Head2|Tail2]):- reverse(Head1,Tail2),reverse(Head2,Tail1).
reverse(L,[_|Tail]):-reverse(L,Tail).
