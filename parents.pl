father(john, david).
father(john, max).
father(david, kieren).
father(david, ben).
father(max, alex).
brother(X, Y) :- father(Z, X), father(Z, Y), \+ X=Y.
cousin(X, Y) :- father(Z, X), father(W, Y), father(Q, Z), father(Q, W), \+ X=Y.
grandson(X, Y) :- father(Y, Z), father(Z, X).
descendent(X, Y) :- father(Y, X); grandson(X, Y).